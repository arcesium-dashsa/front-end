# Sample Front end Service
## About
This is a sample Front end Service acting as a gRPC client and calls the worker service.


## Key Points


- It creates a custom namespace resolver provider extending from  NameResolverProvider class from gRPC package, The class name is KubernetesNameResolverProvider
- Sample client can be found in com.satya.dynamicconfigchange.grpcclient.SampleClient.
- Kubernetes resources can be found in resources/kubernetes directory.
- Endpoints are discovered through fabric8 Kubernetes client API. 
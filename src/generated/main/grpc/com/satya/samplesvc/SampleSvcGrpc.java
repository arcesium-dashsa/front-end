package com.satya.samplesvc;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.35.0)",
    comments = "Source: SampleData.proto")
public final class SampleSvcGrpc {

  private SampleSvcGrpc() {}

  public static final String SERVICE_NAME = "com.arcesium.reconservice.grpcController.SampleSvc";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.satya.samplesvc.Request,
      com.satya.samplesvc.Response> getGreetMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "greet",
      requestType = com.satya.samplesvc.Request.class,
      responseType = com.satya.samplesvc.Response.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.satya.samplesvc.Request,
      com.satya.samplesvc.Response> getGreetMethod() {
    io.grpc.MethodDescriptor<com.satya.samplesvc.Request, com.satya.samplesvc.Response> getGreetMethod;
    if ((getGreetMethod = SampleSvcGrpc.getGreetMethod) == null) {
      synchronized (SampleSvcGrpc.class) {
        if ((getGreetMethod = SampleSvcGrpc.getGreetMethod) == null) {
          SampleSvcGrpc.getGreetMethod = getGreetMethod =
              io.grpc.MethodDescriptor.<com.satya.samplesvc.Request, com.satya.samplesvc.Response>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "greet"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.satya.samplesvc.Request.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.satya.samplesvc.Response.getDefaultInstance()))
              .setSchemaDescriptor(new SampleSvcMethodDescriptorSupplier("greet"))
              .build();
        }
      }
    }
    return getGreetMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SampleSvcStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SampleSvcStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SampleSvcStub>() {
        @java.lang.Override
        public SampleSvcStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SampleSvcStub(channel, callOptions);
        }
      };
    return SampleSvcStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SampleSvcBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SampleSvcBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SampleSvcBlockingStub>() {
        @java.lang.Override
        public SampleSvcBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SampleSvcBlockingStub(channel, callOptions);
        }
      };
    return SampleSvcBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SampleSvcFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SampleSvcFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SampleSvcFutureStub>() {
        @java.lang.Override
        public SampleSvcFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SampleSvcFutureStub(channel, callOptions);
        }
      };
    return SampleSvcFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class SampleSvcImplBase implements io.grpc.BindableService {

    /**
     */
    public void greet(com.satya.samplesvc.Request request,
        io.grpc.stub.StreamObserver<com.satya.samplesvc.Response> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGreetMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGreetMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.satya.samplesvc.Request,
                com.satya.samplesvc.Response>(
                  this, METHODID_GREET)))
          .build();
    }
  }

  /**
   */
  public static final class SampleSvcStub extends io.grpc.stub.AbstractAsyncStub<SampleSvcStub> {
    private SampleSvcStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SampleSvcStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SampleSvcStub(channel, callOptions);
    }

    /**
     */
    public void greet(com.satya.samplesvc.Request request,
        io.grpc.stub.StreamObserver<com.satya.samplesvc.Response> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGreetMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class SampleSvcBlockingStub extends io.grpc.stub.AbstractBlockingStub<SampleSvcBlockingStub> {
    private SampleSvcBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SampleSvcBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SampleSvcBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.satya.samplesvc.Response greet(com.satya.samplesvc.Request request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGreetMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class SampleSvcFutureStub extends io.grpc.stub.AbstractFutureStub<SampleSvcFutureStub> {
    private SampleSvcFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SampleSvcFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SampleSvcFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.satya.samplesvc.Response> greet(
        com.satya.samplesvc.Request request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGreetMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GREET = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SampleSvcImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SampleSvcImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GREET:
          serviceImpl.greet((com.satya.samplesvc.Request) request,
              (io.grpc.stub.StreamObserver<com.satya.samplesvc.Response>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SampleSvcBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SampleSvcBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.satya.samplesvc.SampleDataProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SampleSvc");
    }
  }

  private static final class SampleSvcFileDescriptorSupplier
      extends SampleSvcBaseDescriptorSupplier {
    SampleSvcFileDescriptorSupplier() {}
  }

  private static final class SampleSvcMethodDescriptorSupplier
      extends SampleSvcBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SampleSvcMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SampleSvcGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SampleSvcFileDescriptorSupplier())
              .addMethod(getGreetMethod())
              .build();
        }
      }
    }
    return result;
  }
}

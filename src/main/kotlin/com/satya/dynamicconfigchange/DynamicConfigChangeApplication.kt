package com.satya.dynamicconfigchange

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class DynamicConfigChangeApplication

fun main(args: Array<String>) {
    runApplication<DynamicConfigChangeApplication>(*args)
}

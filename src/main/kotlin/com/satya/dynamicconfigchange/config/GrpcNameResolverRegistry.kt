package com.satya.dynamicconfigchange.config

import io.grpc.NameResolverRegistry
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import javax.annotation.Resource
import javax.annotation.Resources

@Component
class GrpcNameResolverRegistry(nameResolverProvider: KubernetesNameResolverProvider) {
    init {
        val nameResolverRegistry = NameResolverRegistry.getDefaultRegistry()
        nameResolverRegistry.register(nameResolverProvider)
    }
}
package com.satya.dynamicconfigchange.config

import com.google.common.base.Preconditions
import io.grpc.NameResolver
import io.grpc.NameResolverProvider
import io.grpc.NameResolverRegistry
import io.grpc.internal.GrpcUtil
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.net.URI

/**
 * Usage: kubernetes:///{namespace}/{service}/{port}
 * E.g.: kubernetes:///default/echo-server/8080
 */

@Configuration
open class KubernetesNameResolverProviderFactory {
    @Bean
    open fun getNameResolverProvider(): KubernetesNameResolverProvider {
        return KubernetesNameResolverProvider()
    }
}


class KubernetesNameResolverProvider : NameResolverProvider() {
    override fun isAvailable(): Boolean {
        return true
    }

    override fun priority(): Int {
        return 5
    }

    override fun newNameResolver(targetUri: URI, params: NameResolver.Args?): NameResolver? {
        return if (SCHEME == targetUri.scheme) {
            val targetPath = Preconditions.checkNotNull(targetUri.path, "targetPath")
            Preconditions.checkArgument(targetPath.startsWith("/"),
                    "the path component (%s) of the target (%s) must start with '/'", targetPath, targetUri)
            val parts = targetPath.split("/".toRegex()).toTypedArray()
            require(parts.size == 4) { "Must be formatted like kubernetes:///{namespace}/{service}/{port}" }
            try {
                val port = Integer.valueOf(parts[3])
                KubernetesNameResolver(parts[1], parts[2], port, GrpcUtil.TIMER_SERVICE, GrpcUtil.SHARED_CHANNEL_EXECUTOR)
            } catch (e: NumberFormatException) {
                throw IllegalArgumentException("Unable to parse port number", e)
            }
        } else {
            null
        }
    }

    override fun getDefaultScheme(): String {
        return SCHEME
    }

    companion object {
        const val SCHEME = "kubernetes"
    }
}
package com.satya.dynamicconfigchange.config

import io.fabric8.kubernetes.api.model.Endpoints
import io.fabric8.kubernetes.client.DefaultKubernetesClient
import io.fabric8.kubernetes.client.KubernetesClient
import io.fabric8.kubernetes.client.KubernetesClientException
import io.fabric8.kubernetes.client.Watcher
import io.grpc.Attributes
import io.grpc.EquivalentAddressGroup
import io.grpc.NameResolver
import io.grpc.internal.SharedResourceHolder
import java.net.InetSocketAddress
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.ScheduledExecutorService
import javax.annotation.concurrent.GuardedBy


open class KubernetesNameResolver(private val namespace: String, private val name: String,
                                  private val port: Int,
                                  private val timerServiceResource: SharedResourceHolder.Resource<ScheduledExecutorService>,
                                  private val sharedChannelExecutorResource: SharedResourceHolder.Resource<Executor>) : NameResolver() {
    private val kubernetesClient: KubernetesClient
    private var listener: Listener? = null

    @Volatile
    private var refreshing = false

    @Volatile
    private var watching = false
    override fun getServiceAuthority(): String {
        return kubernetesClient.masterUrl.authority
    }

    override fun start(listener: Listener) {
        this.listener = listener
        refresh()
    }

    override fun shutdown() {
        kubernetesClient.close()
    }

    @GuardedBy("this")
    override fun refresh() {
        if (refreshing) return
        try {
            refreshing = true
            val endpoints: Endpoints = kubernetesClient.endpoints().inNamespace(namespace)
                    .withName(name)
                    .get()
            update(endpoints)
            watch()
        } finally {
            refreshing = false
        }
    }

    private fun update(endpoints: Endpoints) {
        val servers: MutableList<EquivalentAddressGroup> = ArrayList()
        if (endpoints.subsets == null) return
        endpoints.subsets.stream().forEach { subset ->
            val matchingPorts: Long = subset.ports.stream().filter { p -> p != null && p.port == port }.count()
            if (matchingPorts > 0) {
                subset.addresses.stream().map { address -> EquivalentAddressGroup(InetSocketAddress(address.getIp(), port)) }.forEach { address -> servers.add(address) }
            }
        }
        listener!!.onAddresses(servers, Attributes.EMPTY)
    }

    @GuardedBy("this")
    protected fun watch() {
        if (watching) return
        watching = true
        kubernetesClient.endpoints().inNamespace(namespace)
                .withName(name)
                .watch(object : Watcher<Endpoints?> {
                    override fun eventReceived(action: Watcher.Action?, endpoints: Endpoints?) {
                        when (action) {
                            Watcher.Action.MODIFIED, Watcher.Action.ADDED -> {
                                endpoints?.let { update(it) }
                                return
                            }
                            Watcher.Action.DELETED -> {
                                listener!!.onAddresses(emptyList(), Attributes.EMPTY)
                                return
                            }
                            else -> return
                        }
                    }

                    override fun onClose(e: KubernetesClientException?) {
                        watching = false
                    }

                })
    }

    init {
        kubernetesClient = DefaultKubernetesClient()
    }
}
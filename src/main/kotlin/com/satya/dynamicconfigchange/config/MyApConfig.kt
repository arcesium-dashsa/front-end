package com.satya.dynamicconfigchange.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Configuration

@ConfigurationProperties(prefix = "myapp")
@ConstructorBinding
class MyApConfig {
    lateinit var name: String
    lateinit var message: String
}
package com.satya.dynamicconfigchange.controller

import com.satya.dynamicconfigchange.config.MyApConfig
import com.satya.dynamicconfigchange.grpcclient.SampleClient
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RefreshScope
class RandomController(var config: MyApConfig , val client: SampleClient ) {
    @GetMapping("/helloWorld")
    fun helloWorld(): String {
        return client.makeRequest()?.response ?: "Default response"
    }
}
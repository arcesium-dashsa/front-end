package com.satya.dynamicconfigchange.grpcclient

import com.satya.dynamicconfigchange.config.GrpcNameResolverRegistry
import com.satya.samplesvc.Request
import com.satya.samplesvc.Response
import com.satya.samplesvc.SampleSvcGrpc
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import org.springframework.stereotype.Component

@Component
class SampleClient( registry: GrpcNameResolverRegistry) {

//    private val channel: ManagedChannel =
//            ManagedChannelBuilder.forTarget("kubernetes:///multi-tenant-ns/worker-grpc/9090")
//                    .defaultLoadBalancingPolicy("round_robin")
//                    .usePlaintext().build()
    private val channel: ManagedChannel =
            ManagedChannelBuilder.forTarget("dns:///worker-grpc.multi-tenant-ns.svc.cluster.local:9090")
                    .defaultLoadBalancingPolicy("round_robin")
                    .usePlaintext().build()
    private val stub = SampleSvcGrpc.newBlockingStub(channel)

    fun makeRequest(): Response? {
      val request = Request.newBuilder().setName("Hello").build()
      return stub.greet(request)
    }

}